package com.jarvis.openhouselive.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.vrtoolkit.cardboard.CardboardActivity;

import org.rajawali3d.cardboard.RajawaliCardboardRenderer;
import org.rajawali3d.cardboard.RajawaliCardboardView;
import org.rajawali3d.materials.Material;
import org.rajawali3d.materials.textures.ATexture;
import org.rajawali3d.materials.textures.Texture;
import org.rajawali3d.math.vector.Vector3;
import org.rajawali3d.primitives.Sphere;

import com.jarvis.openhouselive.AppManager;
import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.model.common.Listing;
import com.jarvis.openhouselive.model.common.Marker;
import com.jarvis.openhouselive.util.Constants;

import java.util.Random;

public class PSActivity extends CardboardActivity {

    private Listing listing = null;
    private static Marker marker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle bundle = getIntent().getExtras();
            listing = AppManager.getObjectFromJson(getIntent().getExtras().getString(Constants.EXTRA_KEY_LISTING), Listing.class);
            if (bundle.containsKey(Constants.EXTRA_KEY_MARKER)) {
                marker = AppManager.getObjectFromJson(getIntent().getExtras().getString(Constants.EXTRA_KEY_MARKER), Marker.class);
            } else {
                for (Marker mrk : listing.getMarkers()) {
                    if (mrk.getX().equalsIgnoreCase("0") && mrk.getY().equalsIgnoreCase("0")) {
                        marker = listing.getMarkers().get(0);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Invalid Data", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        RajawaliCardboardView view = new RajawaliCardboardView(this);
        setContentView(view);
        setCardboardView(view);

        RajawaliCardboardRenderer renderer = new MyRenderer(this); // your renderer
        view.setRenderer(renderer);        // required for CardboardView
        view.setSurfaceRenderer(renderer); // required for RajawaliSurfaceView
    }

    @Override
    public void onCardboardTrigger() {
        super.onCardboardTrigger();
        Random random = new Random();
        int index = random.nextInt(marker.getChildIds().size());
        String childId = marker.getChildIds().get(index == 0 ? marker.getChildIds().size() - 1 : index - 1);
        for(Marker mrk : listing.getMarkers()) {
            if(mrk.getMarkerId().equalsIgnoreCase(childId)) {
                Intent intent = new Intent(PSActivity.this, PSActivity.class);
                intent.putExtras(getIntent().getExtras());
                intent.putExtra(Constants.EXTRA_KEY_MARKER, AppManager.getJsonString(mrk));
                startActivity(intent);
                finish();
                System.exit(0);
            }
        }
    }

    private static class MyRenderer extends RajawaliCardboardRenderer {

        public MyRenderer(Context context) {
            super(context);
        }

        @Override
        protected void initScene() {
            Sphere sphere = createPhotoSphereWithTexture(new Texture("photo", getResourceId(marker.getImageUrl())));
            getCurrentScene().addChild(sphere);
            getCurrentCamera().setPosition(Vector3.ZERO);
            getCurrentCamera().setFieldOfView(75);


        }

        private static Sphere createPhotoSphereWithTexture(ATexture texture) {

            Material material = new Material();
            material.setColor(0);

            try {
                material.addTexture(texture);
            } catch (ATexture.TextureException e) {
                throw new RuntimeException(e);
            }

            Sphere sphere = new Sphere(50, 64, 32);
            sphere.setScaleX(-1);
            sphere.setMaterial(material);

            return sphere;
        }

        public int getResourceId(String url) {
            if (url.contains("p1")) {
                return R.raw.p1;
            } else if (url.contains("p2")) {
                return R.raw.p2;
            } else if (url.contains("p3")) {
                return R.raw.p3;
            } else if (url.contains("p4")) {
                return R.raw.p4;
            } else if (url.contains("p5")) {
                return R.raw.p5;
            }
            return R.raw.p1;
        }
    }
}