package com.jarvis.openhouselive.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jarvis.openhouselive.R;
import com.jarvis.openhouselive.util.Constants;

import butterknife.ButterKnife;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class AboutFragment extends Fragment {

    public static AboutFragment newInstance(Bundle bundle) {
        AboutFragment fragment = new AboutFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static String getMyTag() {
        return Constants.TAG_FRAGMENT_ABOUT;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
