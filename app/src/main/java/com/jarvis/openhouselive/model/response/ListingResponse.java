package com.jarvis.openhouselive.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jarvis.openhouselive.model.common.Listing;

import java.util.ArrayList;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class ListingResponse {

    @Expose
    @SerializedName("listings")
    private ArrayList<Listing> listings;

    public ArrayList<Listing> getListings() {
        return listings;
    }
}
