package com.jarvis.openhouselive.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by meeth.dinesh on 31/10/15.
 */
public class Listing {

    @Expose
    @SerializedName("address")
    private String address;
    @Expose
    @SerializedName("area")
    private String area;
    @Expose
    @SerializedName("locName")
    private String locality;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("rent")
    private String price;
    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("thumb")
    private String imageUrl;
    @Expose
    @SerializedName("sphereUrl")
    private String imagePhotoSphere;

    @Expose
    @SerializedName("amenities")
    private String[] amenities;

    @SerializedName("children")
    private ArrayList<Marker> markers;

    public Listing() {
    }

    public String getAddress() {
        return address;
    }

    public String getArea() {
        return area;
    }

    public String getLocality() {
        return locality;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImagePhotoSphere() {
        return imagePhotoSphere;
    }

    public String[] getAmenities() {
        return amenities;
    }

    public ArrayList<Marker> getMarkers() {
        return markers;
    }
}
