package com.jarvis.openhouselive.model.common;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by meeth.dinesh on 01/11/15.
 */
public class Marker {

    @SerializedName("x")
    private String x;
    @SerializedName("y")
    private String y;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("groupId")
    private String groupId;
    @SerializedName("markerId")
    private String markerId;

    @SerializedName("childId")
    ArrayList<String> childIds;

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getMarkerId() {
        return markerId;
    }

    public ArrayList<String> getChildIds() {
        return childIds;
    }
}
